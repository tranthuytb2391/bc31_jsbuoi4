function demSo() {
    var soThu1 = document.getElementById("txt-so-thu-1").value;
    var soThu2 = document.getElementById("txt-so-thu-2").value;
    var soThu3 = document.getElementById("txt-so-thu-3").value;

    var demChan = 0;
    var demLe = 0;
    var arr = new Array(soThu1, soThu2, soThu3);

    for (var i = 0; i < arr.length; i++) {
        if (arr[i] % 2 == 0) {
            demChan++;
        } else {
            demLe++;
        }
    }
    document.getElementById("ket-qua").innerHTML = "Có " + demChan + " số chẵn,  " + demLe + " số lẻ";

}